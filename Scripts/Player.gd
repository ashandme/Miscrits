extends KinematicBody2D
var movimiento = Vector2()
var speed = 150
func _process(delta):
	movimiento = Vector2()

	if (Input.is_action_pressed("ui_down")):
		movimiento.x = speed
		movimiento.y = speed
		$Sprite.play("behind")
		$Sprite.set_flip_h(true)
		$Sprite.play("Walking")
	elif (Input.is_action_pressed("ui_up")):
		movimiento.x = -speed
		movimiento.y = -speed
		$Sprite.play("behind")
		$Sprite.set_flip_h(false)
		$Sprite.play("behind")
	elif (Input.is_action_pressed("ui_right")):
		movimiento.x = speed
		movimiento.y = -speed
		$Sprite.play("Walking")
		$Sprite.set_flip_h(true)
		$Sprite.play("behind")
	elif (Input.is_action_pressed("ui_left")):
		movimiento.x = -speed
		movimiento.y = speed
		$Sprite.play("Walking")
		$Sprite.set_flip_h(false)
		$Sprite.play("Walking")
	if Input.is_action_pressed("ui_Run"):
		speed = 250
		$Sprite.get_sprite_frames().set_animation_speed("Walking", 10.0)
		speed = 150
		$Sprite.get_sprite_frames().set_animation_speed("Walking", 5.0)
	else:
		speed = 150
		$Sprite.get_sprite_frames().set_animation_speed("Walking", 5.0)
	movimiento = move_and_slide(movimiento)
